public class Board {
 private Die firstDie;
 private Die secondDie;
 private boolean[] closedTiles;
 
 public Board() {
  this.firstDie = new Die();
  this.secondDie = new Die();
  this.closedTiles = new boolean[12];
  for (int i=0;i<this.closedTiles.length ;i++) {
    this.closedTiles[i] = false;
  }
 }
 public String toString() {
   String numbers = "";
   for (int i =0 ; i<this.closedTiles.length ; i++) {
     if (!this.closedTiles[i]) {
       numbers = numbers + (i+1) + " ";
     }
     else {
       
       numbers = numbers + "X ";

     }
   }
   return numbers;
 }
 public boolean playATurn() {
   this.firstDie.roll();
   this.secondDie.roll();
   
   System.out.println(this.firstDie.toString() + "," + this.secondDie.toString());
   int sum = firstDie.getPips() + secondDie.getPips();
   boolean gameOver = false;

   if(this.closedTiles[sum-1]) {
    System.out.println("The tile at this position is already shut");
    gameOver = true;
     }
   
   else {
     this.closedTiles[sum-1] = true;
     System.out.println("Closing Tile: " + (sum));
     gameOver=false;
     }
   
   return gameOver;
   }
 }