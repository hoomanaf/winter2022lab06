public class ShutTheBox {
  public static void main(String args[]) {
    
    System.out.println("Welcome to the Shut The Box game");
    Board board = new Board();
    boolean gameOver = false;
    while (!gameOver) {
      System.out.println("Player 1's turn");
      System.out.println(board.toString());
      boolean result = board.playATurn();
      
      if (!result) {
        System.out.println("Player 2's turn");
        System.out.println(board.toString());
        result = board.playATurn();
        if (result) {
          System.out.println("Player 1 wins");
          gameOver = true;
      }
      }
      else {
        System.out.println("Player 2 wins");
        gameOver = true;
        break;
        }
      }
    System.out.println("Brought to you by: Hooman Afshari-2134814");
    }
    
  }